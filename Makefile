DATA_DIR = "/usr/share/montorix"
STATE_DIR = "/var/lib/monitorix"
SYSTEMD_UNITS_DIR = `pkg-config --variable=systemdsystemunitdir  systemd`
VERSION = 1.0

.PHONY: install

install:
	install -d $(INSTALL_ROOT)/$(SYSTEMD_UNITS_DIR)
	install -d $(INSTALL_ROOT)/$(DATA_DIR)/monitorix/
	install -d $(INSTALL_ROOT)/etc/monitorix/conf.d
	install -d $(INSTALL_ROOT)/$(STATE_DIR)/data

	install -m 0644 monitorix.service $(INSTALL_ROOT)/$(SYSTEMD_UNITS_DIR)/monitorix.service
	
	install -m 0644 config.d/*.conf $(INSTALL_ROOT)/$(DATA_DIR)/monitorix
	

dist:
	mkdir -p monitorix-$(VERSION)/config.d
	cp monitorix.service monitorix-$(VERSION)
	cp config.d/*.conf monitorix-$(VERSION)/config.d
	cp Makefile monitorix-$(VERSION)
	tar -zcvf monitorix-$(VERSION).tar.gz monitorix-$(VERSION)
	rm -rf monitorix-$(VERSION)
