# Monitorix in docker

## Supported Modules

    * System load average and usage (system.rrd)
    * Global kernel usage (kern.rrd)
    * Kernel usage per processor (proc.rrd)
    * LM-Sensors and GPU temperatures (lmsens.rrd)
    * Disk drive temperatures and health (disk.rrd)
    * Network traffic and usage (net.rrd)
    * Netstat statistics (netstat.rrd)
    * Libvirt statistics (libvirt.rrd)
    * Apache statistics (apache.rrd)
    * Nginx statistics (nginx.rrd)
    * Lighttpd statistics (lighttpd.rrd)
    * MySQL statistics (mysql.rrd)
    * Varnish cache statistics (varnish.rrd)
    * PageSpeed Module statistics (pagespeed.rrd)
    * Squid Proxy Web Cache (squid.rrd)
    * BIND statistics (bind.rrd)
    * NTP statistics (ntp.rrd)
    * Icecast Streaming Media Server (icecast.rrd)
    * Alternative PHP Cache statistics (phpapc.rrd)
    * Memcached statistics (memcached.rrd)
    * APC UPS statistics (apcupsd.rrd)
    * Network UPS Tools statistics (nut.rrd)
    * Wowza Media Server (wowza.rrd)
    * Devices interrupt activity (int.rrd)


# Install and run
=============

```
$ sudo make install
$ docker pull geiseri/monitorix
$ systemctl start monitorix
```

# Configuration

All configurations are stored in ```/etc/monitorix/conf.d```.  These files are configuration fragments as documented 
by: http://www.monitorix.org/documentation.html.  Create files in that directory with a .conf suffix so that the 
main configuration file can include them properly.  They are loaded in alpha-numeric order.

